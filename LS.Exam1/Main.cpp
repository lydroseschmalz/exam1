#include <iostream>
#include <conio.h>
#include <cmath>

using namespace std;

float Square(float num);
float Cube(float num);

float Square(float num) 
{
	float answer = pow(num, 2);
	return answer;
}

float Cube(float num) 
{
	float answer = pow(num, 3);
	return answer;
}

int main() 
{
	while(true)
	{
		float num;
		int calculation;

		cout << "\nPlease enter a number: \n";
		cin >> num;
		cout << "\nWould you like to square(2) or cube(3) the number?: \n";
		cout << "\n2. Square\n";
		cout << "\n3. Cube\n\n";

		cin >> calculation;

		switch (calculation)
		{
		case 2:
			cout << "\nAnswer: " << Square(num) << "\n";
			break;
		case 3:
			cout << "\nAnswer: " << Cube(num) << "\n";
			break;
		default:
			cout << "\nPlease select a valid calculation!\n";
		}

		char cont;

		do 
		{
			cout << "\nWould you like to perform another calculation (Y/N)?\n";
			cout << "\nYou must type a 'Y' or an 'N': \n";
			cin >> cont;
		} while ((cont != 'Y') && (cont != 'N') && (cont != 'y') && (cont != 'n'));

		if (cont == 'N' || cont == 'n') return 0;
	}
}